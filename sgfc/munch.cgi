#!/usr/bin/perl

use CGI;

$cgi  = new CGI(\*STDIN);

# parse sgfc commandline options
# does some things more complicated than necessary
# to prevent arbitrary shell execution of evil form content

$options = '-c';
$opt_b = $cgi->param('opt_b');       # search for beginning style
$options .= " -b1" if($opt_b == 1);
$options .= " -b2" if($opt_b == 2);
$options .= " -b3" if($opt_b == 3);
$opt_l = $cgi->param('opt_l');       # linebreak style
$options .= " -l1" if($opt_l == 1);
$options .= " -l2" if($opt_l == 2);
$options .= " -l3" if($opt_l == 3);
$options .= " -l4" if($opt_l == 4);

$options .= " -p" if($cgi->param('opt_p')); # ff3 compatibility stuff
$options .= " -e" if($cgi->param('opt_e'));
$options .= " -t" if($cgi->param('opt_t'));

$options .= " -w" if($cgi->param('opt_w')); # messages
if($cgi->param('opt_d')) {
  $opt_d = $cgi->param('opt_d_val');
  $opt_d =~ s/[^0-9,]//g; # remove all evil chars
  $opt_d =~ s/,/d/g;      # for multiple entries
  $options .= " -d$opt_d";
}

$options .= " -n" if($cgi->param('opt_n')); # delete stuff
$options .= " -o" if($cgi->param('opt_o'));
$options .= " -u" if($cgi->param('opt_u'));
$options .= " -m" if($cgi->param('opt_m'));
if($cgi->param('opt_y')) {
  $opt_y = $cgi->param('opt_y_val');
  $opt_y =~ s/[^A-Z,]//g; # remove all evil chars
  $opt_y =~ s/,/y/g;      # for multiple entries
  $options .= " -y$opt_y";
}

$options .= " -g" if($cgi->param('opt_g')); # miscellaneous
$options .= " -k" if($cgi->param('opt_k'));
$options .= " -v" if($cgi->param('opt_v'));
$options .= " -z" if($cgi->param('opt_z'));
$options .= " -r" if($cgi->param('opt_r'));
$options .= " -h" if($cgi->param('opt_h'));



# print HTML

print <<EOF
Content-type: text/html

<HTML lang="en">
<HEAD>
<TITLE>Munch data through SGFC - results</TITLE>
</HEAD>

<BODY>
<A HREF="../index.html"><IMG SRC="../images/head.png" ALT="[SGF FF[4] - Smart Game Format]" BORDER=0></A>

<H1>Munch data through SGFC - results</H1>

<H2>SGFC results</H2>
<FORM>
<div class=just><TEXTAREA ROWS=12 COLS=75>
EOF
;

print "Options: $options\n\n";

# create temp SGF file

$file = "/tmp/$$-" . int(rand(5000)) . ".sgf";
open(SGF, ">$file");
print SGF $cgi->param('sgf');
close(SGF);

# and run it through SGFC
open(SGFC, "/home/ahollosi/bin/sgfc $options $file $file|");
while(<SGFC>)
{ print $_; }
close (SGFC);

# print resulting SGF file
print <<EOF
</TEXTAREA></div></FORM>

<h2>Resulting SGF</h2>
<pre>
EOF
;

if(!$cgi->param('opt_h'))
{
  open(SGF, "<$file");
  while(<SGF>)
  {
    s/&/&amp;/g;
    s/</&lt;/g;
    print;
  }
  close(SGF);
}

# remove temporary file
unlink($file);

# close HTML
print <<EOF
</pre>
</body>
</html>
EOF
;

exit (0);
