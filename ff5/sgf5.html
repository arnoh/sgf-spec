<!DOCTYPE html>
<html lang="en">
<head>
    <title>SGF file format FF[5] (draft)</title>
    <meta charset="utf-8">
    <meta name="author" content="Arno Hollosi, ahollosi@xmp.net">
    <meta name="keywords" content="sgf,file format,game records,go,weiqi,baduk,paduk">
    <link rel="stylesheet" href="assets/main.css">
</head>
<body>
<header>
    <a href="./">SGF file format FF[5] (draft)</a>
    <h1>FF5: Core specification</h1>
    Last updated: 2016-10-30
</header>
<article>

    <h1>Introduction</h1>

    <p>
        SGF is a text-only format (not a binary format) for game records.
    <p>
        The information is grouped in nodes and stored in properties.
        Nodes are organized as trees. There are no exceptions; if a game needs to
        store some information within SGF, a (game-specific) property must be
        defined for that purpose.
    <p class="robust">
        This specification contains "robustness" hints.
        Those hints are meant <strong>for parsing (reading) only</strong>.
        A compliant application <strong>must not</strong> produce such SGF.
        <br>Robustness principel / Postel's law:
        <i>Be conservative in what you do, be liberal in what you accept from others</i>.

    <h1>Structure of an SGF file</h1>

    <h2>Formal grammar</h2>

    <div class="note">
        <p>
            This specification uses the Augmented Backus-Naur Form (ABNF) to describe grammar as specified in
            <a href="https://tools.ietf.org/rfcmarkup/5234">RFC 5234</a>, with additions for Unicode terminal
            symbols.
        <p>
            A quick summary:
        <ul>
            <li><code>name = definition</code> : defines a grammar rule called <code>name</code></li>
            <li><code>"..."</code> : terminal value in quotation marks</li>
            <li><code>U+1234</code> : terminal character, specified as Unicode code point</li>
            <li><code>[...]</code> : option: value can occur zero or one time</li>
            <li>
                <code>a*b ...</code> : repetition: at least <code>a</code> times, at most <code>b</code> times.
                By default, <code>a = 0</code> and <code>b = infinity</code>, so for example,
                <code>*...</code> means "any, including zero times", <code>1*...</code> means "at least once."
            </li>
            <li><code>(...)</code> : grouping</li>
            <li><code>/</code> : alternative</li>
        </ul>
    </div>

    <p>
        The grammar of the node and tree structure of the SGF file is defined in ABNF as follows:

    <pre class="definition">
Collection          = 1*NodeTree
NodeTree            = <span class="terminal">"("</span> NodeSequence *NodeTree <span class="terminal">")"</span>
NodeSequence        = [<span class="terminal">";"</span>] Node *(<span class="terminal">";"</span> Node)
Node                = *Property
Property            = PropertyIdentifier 1*PropertyValue
PropertyIdentifier  = 1*UppercaseLetter
PropertyValue       = <span class="terminal">"["</span> ValueType <span class="terminal">"]"</span>
UppercaseLetter     = <span class="terminal">"A".."Z"</span>  <span
            class="comment">; i.e. the range U+0041 .. U+005A</span>
</pre>
    <p class="note">
        In a <code>NodeSequence</code> the first <code>";"</code> is optional. This allows for variations that
        are entirely empty, such as <code>(GC[start]()(C[second variation))</code>.
    <p>
        Whitespace (space, tab, carriage return, line feed, vertical tab and so on) may appear anywhere between
        the defined elements (i.e. <code>NodeTree</code>, <code>Node</code>, <code>Property</code>, <code>PropertyIdentifier</code>,
        <code>PropertyValue</code> etc.) and is ignored.
    <pre class="example">(;GM[1];C[1]; AB[aa][bb]  [cc]
   ; ; N [123] ( ; TB
[cc])  (;TW[cc]))
    </pre>

    <div class="robust">
        <p>Omitting the first <code>";"</code> in a <code>NodeSequence</code> was not allowed in earlier versions of SGF.
            It is therefore strongly recommended to add this optional semicolon when writing SGF files in order
            to increase compatibility.
        </p>

        <p>Older SGF specifications allowed lowercase letters in property identifiers.</p>
        <pre><strong>Found:</strong>
(;Comment[text](;Black[bb];White[cc];canBeanything[dd])
<strong>Should parse as:</strong>
(;C[text];B[bb];W[cc];B[dd])</pre>

        <p>Older SGF specifications allowed any kind of text in front of the first <code>(;</code>.</p>
        <pre><strong>Found:</strong>
Here is some text, probably directly from a mail (;GC[start here])
<strong>Should parse as:</strong>
(;GC[start here])</pre>
    </div>

    <p>
        The grammar above defines the bare syntax structure of SGF. Often, we need to specify an additional
        semantic constraint, namely that all values of a <code>1*PropertyValue</code> have to be of the same type, we
        enhance the ABNF rules as follows:
    <pre class="definition">
ListOf&lt;x&gt;  = 1*PropertyValue   <span class="comment">; with the requirement that each PropertyValue has the same ValueType &lt;x&gt;</span>
EListOf&lt;x&gt; = ListOf&lt;x&gt; / "[]"  <span class="comment">; which contains the empty list too</span>
</pre>
    <p class="note">
        Almost all SGF properties are actually requiring the same <code>ValueType</code> for their property values,
        so these additional ABNF rules make it easy to specify and discuss this requirement.

    <h2>Properties</h2>

    <p>
        Property identifiers are defined using only uppercase letters.
        The specification itself only defines property identifiers with one or two uppercase letters.
        However applications should be able to handle property identifiers of any (reasonable) length.
    <p>
        The order of properties within a node is not fixed. It may change every time
        the SGF file is saved and may vary from application to application.
        In addition, applications should <strong>not</strong> rely on the order of property
        values. Note that the order of values might change as well.
    <p>
        Only <strong>one</strong> of each property is allowed per node, e.g. one cannot have two
        comments in one node: <code>... ; C[comment1] B [dg] C[comment2] ; ...</code>
        This is an error.
    <p class="robust">
        Some malformed files sometimes contain more than one of each property in a node.
        Typically, a good strategy is to concatenate multiple <code>SimpleText</code>, <code>Text</code>,
        and <code>ListOf&lt;x&gt;</code> values to one single value (or list). For all other types or
        well-known text values (e.g. <code>RE</code>) it is recommended to choose the first occurance
        and discard all other duplicates.
        Inside a list (e.g. <code>AB</code> or <code>LB</code>) duplicates usually can just be deleted.

    <h3 id="2.2.4">Private Properties</h3>
    <p>
        Applications may define their own private properties. The following restrictions apply:
    <p><em>Property identifier:</em> private properties <strong>must not</strong> use an
        identifier used by one of the standard properties. You have to use a new
        identifier instead.
    <p><em>Property value:</em> private properties may use one of the value types
        defined in this document or define their own value type. When using
        a private value type it is mandatory to adhere to the <a href="#text"><code>Text</code></a> value
        escaping rules (about escaping <code>"]"</code>, and <code>"\"</code>).
        Otherwise the file would become unparseable. Should the value type be a combination of two simpler types
        then it is recommended that your application uses the <code>ComposedValue</code> type.
    </p>
    <div class="robust">
        <p>How to handle unknown/faulty properties:</p>
        <ul>
            <li>Unknown properties and their values should be preserved. While parsing property values
                treat them as if they are of type <code>Text</code>.
            </li>
            <li>Illegally formatted known game-info property values should be corrected if possible, otherwise
                preserved.
            </li>
            <li>Other illegally formatted known properties should be corrected if possible, otherwise deleted.</li>
        </ul>
    </div>
    <h2>Tree structure</h2>
    <p>
        A <code>GameTree</code> is stored <strong>in pre-order</strong>. This makes the first line the "main line of
        play."
    </p>
    <div class="example">
        <table>
            <tr>
                <td style="padding-right:4em">
                    <img src="assets/tree-example1.svg" height=250 alt="example tree">
                    <br>Example for tree structure
                <td style="padding-right:4em">
                    <img src="assets/tree-example2.gif" height=203 alt="user view of tree">
                    <br>Tree as displayed in a sample application.
                </td>
                <td>
                    <h4>Corresponding SGF example</h4>
                    <pre>
(;FF[4]C[root](;C[a];C[b](;C[c])
(;C[d];C[e]))
(;C[f](;C[g];C[h];C[i])
(;C[j])))
</pre>
                    <h4>Example algorithm to write a tree in pre-order:</h4>
                    <pre>
WriteTree(Root)
End

WriteTree(Node)
   Write(Node)
   for each child of Node
      WriteTree(child)
   end for
end
                    </pre>
                </td>
            </tr>
        </table>
    </div>

    <p class="note">
        Applications which are not able to handle variations can therefore ignore all <code>GameTree</code> symbols
        and stop reading at the first <code>)</code>.

    <h3>Node numbering</h3>
    <p>
        When numbering nodes starting with zero is recommended. Nodes should be
        numbered in the way they are stored in the file.
    <p class="example">
        Example numbering for the SGF file from above:
        <code>root=0, a=1, b=2, c=3, d=4, e=5, f=6, g=7,h=8, i=9 and j=10.</code>
    </p>

    <h1>Character encoding</h1>

    <p>
        Information can be stored in different byte-encodings. While the SGF nodes and property identifiers
        only use a subset of ASCII characters, property values may contain any characters.
    </p>
    <div class="note">
        For this specification, we usually use the following terms:
        <dl>
            <dt>Character</dt>
            <dd>The smallest component of written language that has semantic value; refers to the abstract meaning;
                it is the basic unit we are talking about.
            </dd>
            <dt>Charset</dt>
            <dd>A character set in which each character is assigned a numeric code point.</dd>
            <dt>Encoding</dt>
            <dd>A byte serialization ("on disk") for a defined charset.</dd>
        </dl>
        Please note that older charsets combine charset+encoding in one step.
        <p>
            Example: The <em>character</em> <code>"ü"</code> is assigned the value 252 in the
            <em>charset</em> ISO-8859-1 and encoded as the byte sequence <code>fc</code> in the ISO-8859-1
            <em>encoding</em>.
            The same <em>character</em> is assigned the value U+00FC in the <em>charset</em> Unicode and encoded as
            the byte sequence <code>c3 bc</code> in the UTF-8 <em>encoding</em>, and encoded as the byte sequence
            <code>a8 b9</code> in the GB18030 <em>encoding</em>.
    </div>

    <h2>Default rules and <code>CA</code> property</h2>
    <p>
        The default charset of SGF is <strong>Unicode</strong>, the default encoding is <strong>UTF-8</strong>.
    <p>
        The <a href="../properties.html#CA"><code>CA</code></a> property can be used to define a different charset and
        encoding for the
        SGF file. It can <strong>only</strong> specify encodings which are ASCII compatible, meaning, where the
        encoding of the ASCII characters (<code>U+0000 - U+007F</code>) are encoded as
        the same single bytes (byte range <code>0x00-0x7f</code>) as they are in ASCII (see examples below.)
    <p>
        As encodings may use multi-byte sequences which may contain bytes in the range <code>0x00-0x7F</code>,
        it is mandatory that the <a href="../properties.html#CA"><code>CA</code></a> property is defined
        <strong>before</strong> any such property values appear in the
        SGF file.
    <p class="note">
        This is contradictory to the general "there is no order of properties in a node". But to
        ease parsing, it makes sense to mandate that the <a href="../properties.html#CA"><code>CA</code></a> property comes
        first.
    <p class="robust">
        Older SGF specifications had <code>ISO-8859-1</code> as default character set. It is therefore recommended
        to treat <code>FF[1..4]</code> files without <a href="../properties.html#CA"><code>CA</code></a> property as
        encoded in that character set.
    <p class="todo">
        Specify (link to) list of allowed charsets names and aliases for <a
            href="../properties.html#CA"><code>CA</code></a> property; specify that name
        is not case sensitve.

    <h2>How to parse with respect to encoding</h2>

    <p>Following steps should be followed, when parsing an SGF file:</p>
    <ol>
        <li>Start parsing with the assumption that the charset is Unicode and the encoding is UTF-8.</li>
        <li>If the <a href="../properties.html#CA"><code>CA</code></a> property is encountered, switch charset and encoding
            to the value specified in
            the <a href="../properties.html#CA"><code>CA</code></a> property.
        </li>
    </ol>

    <div class="example">
        <table class="top">
            <tr>
                <th>Example</th>
                <th>Characters</th>
                <th>Byte sequence</th>
                <th>Comment</th>
            </tr>
            <tr>
                <th><a href="assets/encoding-utf8.sgf">Unicode + UTF-8</a></th>
                <td>(;FF[5]C[Hübsch!]PW[坂田 栄男]EV[名人])</td>
                <td><pre>28 3b 46 46 5b 35 5d 43  5b 48 c3 bc 62 73 63 68
21 5d 50 57 5b e5 9d 82  e7 94 b0 20 e6 a0 84 e7
94 b7 5d 45 56 5b e5 90  8d e4 ba ba 5d 29</pre>
                </td>
                <td>No <a href="../properties.html#CA"><code>CA</code></a> property given, the default UTF-8 encoding is
                    assumed.
                    Note that "名人" translates into the byte sequence <code>e5 90 8d e4 ba ba</code>.
                </td>
            </tr>
            <tr>
                <th><a href="assets/encoding-sjis.sgf">Japanse Shift_JIS</a></th>
                <td>(;FF[5]CA[SJIS]PW[坂田 栄男]EV[名人])</td>
                <td><pre>28 3b 46 46 5b 35 5d 43  41 5b 53 4a 49 53 5d 50
57 5b 8d e2 93 63 20 89  68 92 6a 5d 45 56 5b 96
bc 90 6c 5d 29</pre>
                </td>
                <td>The given encoding Shift_JIS encodes ASCII characters in the same byte sequence as
                    UTF-8 does. Hence, the first 8 bytes are the same and it is safe to start
                    parsing with the assumption of UTF-8. Once the <a href="../properties.html#CA"><code>CA</code></a>
                    property is encountered, the application
                    should switch charset+encoding as specified. Note that "名人" translates into the byte sequence
                    <code>96 bc 90 6c</code>.
                </td>
            </tr>
            <tr>
                <th><a href="assets/encoding-gb18030.sgf">Chinese Simplified (GB18030)</a></th>
                <td>(;FF[5]CA[GB18030]PW[坂田 栄男]EV[名人])</td>
                <td><pre>28 3b 46 46 5b 35 5d 43  41 5b 47 42 31 38 30 33
30 5d 50 57 5b db e0 cc  ef 20 96 d1 c4 d0 5d 45
56 5b c3 fb c8 cb 5d 29</pre>
                </td>
                <td>The given encoding GB18030 encodes ASCII characters in the same byte sequence as
                    UTF-8 does. Hence, the first 8 bytes are the same again.
                    Note that "名人" translates into the byte sequence <code>c3 fb c8 cb</code>.
                </td>
            </tr>
        </table>
    </div>
    <p class="robust">
        For additional robustness, you could re-parse the entire SGF file upon encountering the
        <a href="../properties.html#CA"><code>CA</code></a> property. This is just in case that other <code>Text</code>
        properties were defined before the <a href="../properties.html#CA"><code>CA</code></a> property. Re-parsing
        should be cheap, as the encoding is defined in the <code>FirstNode</code> of the very first
        <code>NodeTree</code>.

    <h1>Property value types</h1>

    <p>
        Properties values are of different types. The ABNF specification is as follows:
    <pre class="definition">
ValueType     = SingleValue / ComposedValue
SingleValue   = None / Number / Real / Double / Color / SimpleText / Text / Point / Move / Stone
ComposedValue = SingleValue <span class="terminal">":"</span> SingleValue

CR              = <span class="terminal">U+000D</span>  <span class="comment">; carriage return</span>
LF              = <span class="terminal">U+000A</span>  <span class="comment">; line feed</span>
Digit           = <span class="terminal">"0"</span>..<span class="terminal">"9"</span>  <span class="comment">; i.e. the range U+0030 .. U+0039</span>
AnyCharacter    = <span class="terminal">U+0000</span> .. <span class="terminal">U+10FFFF</span>

None       = ""  <span class="comment">; i.e. empty, no character</span>
Number     = [<span class="terminal">"+"</span> / <span class="terminal">"-"</span>] 1*Digit
Real       = Number [<span class="terminal">"."</span> 1*Digit]
Double     = (<span class="terminal">"1"</span> / <span class="terminal">"2"</span>)
Color      = (<span class="terminal">"B"</span> / <span class="terminal">"W"</span>)
SimpleText = *AnyCharacter  <span class="comment">; handling see below</span>
Text       = *AnyCharacter  <span class="comment">; handling see below</span>

Point      = <span class="comment">; game-specific</span>
Move       = <span class="comment">; game-specific</span>
Stone      = <span class="comment">; game-specific</span>
</pre>

    <h2 id="double">Double</h2>
    <p>
        Double values are used for annotation properties. They are called Double
        because the value is either simple or emphasized.
        A value of <code>1</code> means 'normal'; <code>2</code> means that it is emphasized.

    <p class="example">
        <code>GB[1]</code> could be displayed as: Good for black
        <br><code>GB[2]</code> could be displayed as: Very good for black

    <h2 id="text">Text</h2>
    <p>
        <code>Text</code> is a formatted, possibly multi-line text. White spaces other than linebreaks
        are converted to space (e.g. this means that tab, vertical tab etc. are converted to space).
        Applications must be able to handle <code>Text</code> values of any size.

    <h3>Line breaks</h3>
    <p>
        A single linebreak is represented differently on different operating systems, e.g.
        <code>CR+LF</code> for Windows, <code>LF</code> on Unix.
        An application should be able to deal with following linebreaks:
        LF, CR, CR+LF.
    <p>
        There are two types of linebreaks, soft and hard:
    <ul>
        <li>A <em>soft line break</em> is a linebreak preceded by a backslash <code>"\"</code> (<code>U+005C</code>).
            A soft linebreak is treated as if it is not present, i.e. it is removed during parsing.
        </li>
        <li>A <em>hard line breaks</em> is any other linebreak (i.e. not preceeded by a backslash).</li>
    </ul>
    <p class="note">
        The text should be displayed the way it is, though long lines may be
        word-wrapped, if they don't fit the display.

    <h3>Escaping</h3>
    <p>
        The backslash <code>"\"</code> (<code>U+005C</code>) is the escape character. Any character following
        <code>"\"</code> is inserted verbatim with the exception that whitespaces still have to
        be converted to space.
    <p>
        It is mandatory to escape the following characters, when used in <code>Text</code>:
    <ul>
        <li><code>"]"</code> (closing square bracket, which indicates the end of the property value),</li>
        <li>and <code>"\"</code> (backslash itself).</li>
    </ul>
    <p class="robust">
        It bears repeating that although not necessary or illegal, other characters may be escaped too.
        E.g. <code>C[thi\s\ \\see\m\s\, is\: to\o\ much]</code> should be written as
        <code>C[this \\seems, is: too much]</code>. Some applications actually choke on unescaped colons (due
        to a false interpretation of older SGF specifications), so your application might consider escaping them
        all the time. But keep in mind: unescaped colons are allowed!
    <p>
        Escaping has to be done at the character level, <strong>not</strong> at the byte/encoding level.
    </p>
    <div class="example">
        <p>
            The player name "申真ソ" is encoded as <code>90 5c 90 5e 83 5c</code> in SJIS.
            Note that the last byte of the encoding is <code>5c</code> which - if it were treated as ASCII byte -
            is the backslash. So in order to parse <code>CA[SJIS]PB[申真ソ]</code> correctly: first decode byte sequence
            to characters according to <a href="../properties.html#CA"><code>CA</code></a> property, only then (at the
            character level) apply escaping rules.
        <p>
            The title or tournament name Honinbo "本因坊" is encoded as <code>a5 bb a6 5d a7 7b</code> in BIG5.
            The <code>5d</code> in the middle (would be <code>"]"</code> if treated as ASCII) does <strong>not</strong>
            need escaping, because again charset decoding has to be performed before escaping rules are applied.
    </div>

    <h2 id="simpletext">SimpleText</h2>
    <p>
        <code>SimpleText</code> is a simple string. All whitespaces other than space must be
        converted to space, i.e. there's no newline! Applications must be able
        to handle SimpleTexts of any size. The intention of <code>SimpleText</code> are values that are usually
        shown in a single line. Applications must be able to handle <code>SimpleText</code> values of any size.
    <h3>Line breaks</h3>
    <p>
        Linebreaks preceded by a <code>"\"</code> (soft linebreaks) are removed during parsing (same as Text type).
        All other linebreaks (hard linebreaks) are converted to space (no newline on display!).
    <h3>Escaping</h3>
    <p>
        Escaping rules are the same as for the <code>Text</code> type): <code>"\"</code> is the escape
        character. Any character following <code>"\"</code> is inserted verbatim with the exception that whitespaces
        still have to be converted to space. Following characters have to be escaped, when used in
        <code>SimpleText</code>: <code>"]"</code> and <code>"\"</code>.

    <h2 id="stone">Stone</h2>
    <p>
        This type is used to specify the point and the piece that should be placed at
        that point. If a game doesn't have a distinguishable set of pieces (figures)
        like e.g. Go (<code>GM[1]</code>) the <code>Stone</code> type is reduced to the <code>Point</code> type below.
    <p class="note">
        If <code>Stone</code> and <code>Point</code> are identical, then a property value of type
        <code>*Stone</code> allows allows compressed point lists too.
    <p>
        For the following games, <code>Stone</code> and <code>Point</code> are identical: Go, Othello, Gomuku, Renju,
        Backgammon, Lines of Action, Hex, Gess.

    <h2 id="move/pos">Move / Point</h2>

    These two types are game specific. Further specifications are available:
    <ul>
        <li><a href="../go.html#types">Go</a>
        <li><a href="../backgammon.html#types">Backgammon</a>
        <li><a href="../loa.html#types">Lines of Action</a>
        <li><a href="../hex.html#types">Hex</a>
        <li><a href="../amazons.html#types">Amazons</a>
        <li><a href="../gess.html#types">Gess</a>
        <li><a href="../octi.html#types">Octi</a>
    </ul>

    <h3 id="3.5.1">Compressed point lists</h3>
    <p>
        If a property takes a list of <code>Point</code> values, then the list may be compressed.
        Compressing is done by specifying rectangles instead of listing every single point in the rectangle.
        Rectangles are specified by using the upper left and lower right corner of the rectangle.
    <p>
        For this purpose, we refine the ABNF specification as follows:
    <pre class="definition">
ListOf&lt;Point&gt; = 1*(<span class="terminal">"["</span> ListPointValue <span class="terminal">"]"</span>)
ListPointValue   = Point / CompressedPoints
CompressedPoints = Point <span class="terminal">":"</span> Point  <span
            class="comment">; composed type value of points</span>
</pre>
    <p>
        For the <code>CompressedPoints</code> value the first point specifies the upper left corner,
        the second point the lower right corner of the rectangle.
        1x1 Rectangles are illegal - they've to be listed as single point.
        The definition of 'point list' allows both single point (e.g. <code>[fe]</code>) and rectangle
        (e.g. <code>[ul:lr]</code>) specifiers in any order and combination. However the points have
        to be unique, i.e. overlap and duplication are forbidden.
    <p>
        To get an idea have a look at an <a href="../DD_VW.html">example</a>.
</article>
</body>
</html>
